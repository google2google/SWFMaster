#ifndef TagTreeItem_h__
#define TagTreeItem_h__

#include "SWFTreeModel.h"
#include "swf/SWFTag.h"

class SWFTagWrapper;

class TagTreeItem
	: public SWFTreeItem
{
private:
	SWFTagWrapper* _tag;
public:
	TagTreeItem(SWFTagWrapper*);
	inline SWFTagWrapper* getTag() const { return _tag; }

	virtual QMenu* requestContextMenu() override;
};
#endif // TagTreeItem_h__
