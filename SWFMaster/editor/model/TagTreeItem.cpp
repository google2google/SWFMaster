#include "TagTreeItem.h"
#include "swf/SWFTagWrapper.h"

TagTreeItem::TagTreeItem( SWFTagWrapper* tag )
	: _tag(tag)
{
	this->setKind(SWFTreeItemKind::Tag);
	this->setName(tag->getTagName());
	this->setUserData(tag);
}

QMenu* TagTreeItem::requestContextMenu()
{
	QMenu* menu = new QMenu();
	//menu->addAction()
	return menu;
}
