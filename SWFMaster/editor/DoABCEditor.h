#ifndef DoABCEditor_h__
#define DoABCEditor_h__

#include "Editor.h"

class TagDoABC;

class DoABCEditor : public Editor
{
private:
	TagDoABC* _abcRef;
public:
	DoABCEditor(TagDoABC* abc);

};

#endif // DoABCEditor_h__
