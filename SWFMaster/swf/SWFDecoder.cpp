#include "lzma/LzmaLib.h"
#include "SWFDecoder.h"
#include "SWF.h"
#include <assert.h>
#include "QtZlib/zlib.h"
#include "SWFTagFactory.h"
#include "TagDecoder.h"

SWFDecoder::SWFDecoder()
	: _swf(nullptr)
	, _stream(nullptr)
	, _complete(false)
{
	_frame = SWFFrame::create();
	_frame->retain();
}

SWFDecoder::~SWFDecoder()
{
	if (_frame)
		_frame->release();
	_frame = nullptr;
}

SWF* SWFDecoder::decode( const std::string& file )
{
	FILE* filePtr;
	fopen_s(&filePtr, file.c_str(), "rb");
	fseek(filePtr, 0, SEEK_END);
	long fileSize = ftell(filePtr);
	char* _data = (char*) malloc(fileSize);
	fseek(filePtr, 0, SEEK_SET);

	size_t totalRead = fread(_data, 1, fileSize, filePtr);
	fclose(filePtr);

	SWFInputStream stream(_data, totalRead);
	_stream = &stream;

	_swf = SWF::create();
	//read header
	char signature[3];
	stream.readBytes(signature, 3);
	_swf->version = stream.readUI8();
	assert(_swf->version >= 9);
	UI32 fileLen = stream.readUI32();

	//LZMA
	if (signature[0] == 'Z')
	{
		_swf->compression = SWFCompression::LZMA;

		char propData[5];
		size_t srcLen = stream.readUI32();
		stream.readBytes(propData, 5);
		srcLen = stream.getRemain();
		char* srcData = (char*) malloc(srcLen);
		stream.readBytes(srcData, srcLen);

		//uncompress
		size_t destLen = fileLen - 8;
		char* dest = (char*) malloc(destLen);
		int result = LzmaUncompress((unsigned char*) dest, &destLen, (unsigned char*) srcData, &srcLen, (unsigned char*) propData, 5);

		free(srcData);
		stream = SWFInputStream(dest, destLen);
	}
	//ZIP
	else if (signature[0] == 'C')
	{
		_swf->compression = SWFCompression::Zlib;

		size_t srcLen = stream.getRemain();
		char* srcData = (char*) malloc(srcLen);
		stream.readBytes(srcData, srcLen);
		z_uLongf desLen = fileLen - 8;
		char* desData = (char*) malloc(desLen);

		uncompress((z_Bytef*) desData, &desLen, (z_Bytef*) srcData, srcLen);
		free(srcData);
		stream = SWFInputStream(desData, desLen);
	}
	else
	{
		_swf->compression = SWFCompression::None;
	}

	//SWF的显示大小
	Rect frameSize = stream.readRect();
	_swf->width = frameSize.Xmax / 20;
	_swf->height = frameSize.Ymax / 20;
	//帧率
	UI16 frameRate = stream.readUI16();
	_swf->frameRate = frameRate >> 8;
	UI16 frameCount = stream.readUI16();

	TagDecoder tagDecoder(&stream, this);
	tagDecoder.decode();

	_stream = nullptr;
	_complete = true;

	return _swf;
}

SWF* SWFDecoder::getSWF() const
{
	return _swf;
}

void SWFDecoder::addDefineTag(UI16 charId, DefineTag* tag)
{
	_dict[charId] = tag;
}

SWFTagRef* SWFDecoder::getDefineTagReference(UI16 charId)
{
	auto itr = _dict.find(charId);
	if (itr == _dict.end())
		return nullptr;

	auto itr2 = _tag2ref.find(charId);
	if (itr2 == _tag2ref.end())
	{
		SWFTagRef* r = SWFTagRef::create(itr->second);
		_tag2ref[charId] = r;
		return r;
	}

	return itr2->second;
}

void SWFDecoder::doABC( TagDoABC* tag )
{
	_frame->addDoABCTag(tag);
}

void SWFDecoder::any( SWFTag* tag )
{

}

void SWFDecoder::unknow( TagUnknow* tag )
{
	
}

void SWFDecoder::metadata( TagMetadata* tag )
{
	_swf->setMetadataTag(tag);
}

void SWFDecoder::frameLabel( TagFrameLabel* tag )
{
	_frame->setFrameLabel(tag);
}

void SWFDecoder::setBackgroundColor( TagSetBackgroundColor* tag )
{
	_swf->setBackgroundColorTag(tag);
}

void SWFDecoder::fileAttributes( TagFileAttributes* tag )
{
	_swf->setFileAttributesTag(tag);
}

void SWFDecoder::end( TagEnd* tag )
{
	
}

void SWFDecoder::scriptLimits( TagScriptLimits* tag )
{
	_swf->setScriptLimitsTag(tag);
}

void SWFDecoder::symbolClass( TagSymbolClass* tag )
{
	_frame->setSymbolClass(tag);
}

void SWFDecoder::importAssets2( TagImportAssets2* tag )
{
	assert(false);
}

void SWFDecoder::protect( TagProtect* tag )
{
	_swf->setProtectTag(tag);
}

void SWFDecoder::exportAssets( TagExportAssets* tag )
{
	
}

void SWFDecoder::enableDebugger( TagEnableDebugger* tag )
{
	_swf->setEnableDebuggerTag(tag);
}

void SWFDecoder::enableDebugger2( TagEnableDebugger2* tag )
{
	_swf->setEnableDebuggerTag2(tag);
}

void SWFDecoder::setTabIndex( TagSetTabIndex* tag )
{

}

void SWFDecoder::defineScalingGrid( TagDefineScalingGrid* tag )
{
	this->addDefineTag(tag->characterID, tag);
	_frame->addDefineTag(tag);
}

void SWFDecoder::defineSceneAndFrameLabelData( TagDefineSceneAndFrameLabelData* tag )
{
	_frame->addDefineTag(tag);
}

void SWFDecoder::defineShape( TagDefineShape* tag )
{
	this->addDefineTag(tag->characterID, tag);
	_frame->addShapeTag(tag);
}

void SWFDecoder::defineShape2( TagDefineShape2* tag )
{
	this->defineShape(tag);
}

void SWFDecoder::defineShape3( TagDefineShape3* tag )
{
	this->defineShape(tag);
}

void SWFDecoder::defineShape4( TagDefineShape4* tag )
{
	this->addDefineTag(tag->characterID, tag);
	_frame->addShapeTag(tag);
}

void SWFDecoder::defineButton( TagDefineButton* tag )
{
	assert(false);
	//TODO: refTags
	_frame->addButtonTag(tag);
}

void SWFDecoder::defineButton2( TagDefineButton2* tag )
{
	this->addDefineTag(tag->characterID, tag);
	_frame->addButtonTag(tag);
}

void SWFDecoder::defineButtonCxform( TagDefineButtonCxform* tag )
{
	_frame->addDefineTag(tag);
}

void SWFDecoder::defineButtonSound( TagDefineButtonSound* tag )
{
	_frame->addDefineTag(tag);
}

void SWFDecoder::defineSprite( TagDefineSprite* tag )
{
	this->addDefineTag(tag->characterID, tag);
	_frame->addSpriteTag(tag);
}

void SWFDecoder::defineMorphShape( TagDefineMorphShape* tag )
{
	this->addDefineTag(tag->characterID, tag);
	_frame->addMorphShapeTag(tag);
}

void SWFDecoder::defineMorphShape2( TagDefineMorphShape2* tag )
{
	this->addDefineTag(tag->characterID, tag);
	_frame->addMorphShapeTag(tag);
}

void SWFDecoder::defineFont( TagDefineFont* tag )
{
	assert(false);
	//TODO: tagRef
	//TODO: addDefineTag
	_frame->addFontTag(tag);
}

void SWFDecoder::defineFont2( TagDefineFont2* tag )
{
	assert(false);
	//TODO: tagRef
	//TODO: addDefineTag
	_frame->addFontTag(tag);
}

void SWFDecoder::defineFont3( TagDefineFont3* tag )
{
	this->addDefineTag(tag->characterID, tag);
	_frame->addFontTag(tag);
}

void SWFDecoder::defineFont4( TagDefineFont4* tag )
{
	assert(false);
	//TODO: addDefineTag
	_frame->addFontTag(tag);
}

void SWFDecoder::defineFontInfo( TagDefineFontInfo* tag )
{
	assert(false);
	//TODO: addDefineTag
	_frame->addDefineTag(tag);
}

void SWFDecoder::defineFontInfo2( TagDefineFontInfo2* tag )
{
	assert(false);
	//TODO: addDefineTag
	_frame->addDefineTag(tag);
}

void SWFDecoder::defineFontAlignZones( TagDefineFontAlignZones* tag )
{
	_frame->addControlTag(tag);
}

void SWFDecoder::defineFontName( TagDefineFontName* tag )
{
	tag->putReference(tag->fontCharacter);
	_frame->addControlTag(tag);
}

void SWFDecoder::defineText( TagDefineText* tag )
{
	this->addDefineTag(tag->characterID, tag);
	_frame->addTextTag(tag);
}

void SWFDecoder::defineText2( TagDefineText2* tag )
{
	assert(false);
	//TODO: tagRef
	//TODO: addDefineTag
	_frame->addTextTag(tag);
}

void SWFDecoder::defineEditText( TagDefineEditText* tag )
{
	this->addDefineTag(tag->characterID, tag);
	_frame->addTextTag(tag);
}

void SWFDecoder::cSMTextSettings( TagCSMTextSettings* tag )
{
	_frame->addControlTag(tag);
}

void SWFDecoder::showFrame( TagShowFrame* tag )
{
	_swf->addFrame(_frame);
	_frame->release();
	_frame = SWFFrame::create();
	_frame->retain();
}

void SWFDecoder::placeObject( TagPlaceObject* tag )
{
	_frame->addControlTag(tag);
}

void SWFDecoder::placeObject2( TagPlaceObject2* tag )
{
	_frame->addControlTag(tag);
}

void SWFDecoder::placeObject3( TagPlaceObject3* tag )
{
	_frame->addControlTag(tag);
}

void SWFDecoder::defineBitsJPEG2( TagDefineBitsJPEG2* tag )
{
	this->addDefineTag(tag->characterID, tag);
	_frame->addBitmapTag(tag);
}

void SWFDecoder::defineBitsJPEG3( TagDefineBitsJPEG3* tag )
{
	this->addDefineTag(tag->characterID, tag);
	_frame->addBitmapTag(tag);
}

void SWFDecoder::defineBinaryData( TagDefineBinaryData* tag )
{
	this->addDefineTag(tag->characterID, tag);
	_frame->addBinaryDataTag(tag);
}

void SWFDecoder::productInfo( TagProductInfo* tag )
{
	_swf->setProductInfoTag(tag);
}

void SWFDecoder::removeObject(TagRemoveObject* tag)
{
	_frame->addControlTag(tag);
}

void SWFDecoder::removeObject2(TagRemoveObject2* tag)
{
	_frame->addControlTag(tag);
}

void SWFDecoder::defineBitsLossless( TagDefineBitsLossless* tag )
{
	this->addDefineTag(tag->characterID, tag);
	_frame->addBitmapTag(tag);
}

void SWFDecoder::defineBitsLossless2( TagDefineBitsLossless2* tag )
{
	this->addDefineTag(tag->characterID, tag);
	_frame->addBitmapTag(tag);
}

void SWFDecoder::defineSound( TagDefineSound* tag )
{
	this->addDefineTag(tag->characterID, tag);
	_frame->addSoundTag(tag);
}

float SWFDecoder::getProgress()
{
	if (_complete)
		return 1;
	if (_stream)
		return _stream->getPosition() / (float)_stream->getLength();
	return 0;
}
