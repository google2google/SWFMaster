#ifndef __SWF_H__
#define __SWF_H__

#include "Object.h"
#include "SWFTag.h"
#include "SWFTypes.h"
#include <vector>
#include <string>

class TagDoABC;
class SWFFrame;
class SWFFrameWrapper;
class SWFWrapper;

// SWF ѹ����ʽ
enum class SWFCompression
{
	None,
	Zlib,
	LZMA
};

class SWF
	: public Object
{
	friend class SWFWrapper;
public:
	SWFCompression compression;
	UI8 version;
	UI16 frameRate;
	UI16 width;
	UI16 height;
private:
	TagFileAttributes* fileAttributes;
	TagEnableDebugger* enableDebugger;
	TagEnableDebugger2* enableDebugger2;
	TagMetadata* metadata;
	TagScriptLimits* scriptLimits;
	TagSetBackgroundColor* setBackgroundColor;
	TagDefineSceneAndFrameLabelData* defineSceneAndFrameLabelData;
	TagProductInfo* productInfo;
	TagProtect* protect;
private:
	std::vector<SWFFrame*> frames;
protected:
	SWF();
public:
	CREATE_FUNC(SWF);
	~SWF();
public:
	void setFileAttributesTag(TagFileAttributes* tag);
	inline TagFileAttributes* getFileAttributesTag() const { return fileAttributes; }

	void setMetadataTag(TagMetadata* tag);
	inline TagMetadata* getMetadataTag() const { return metadata; }

	void setBackgroundColorTag(TagSetBackgroundColor* tag);
	inline TagSetBackgroundColor* getBackgroundColorTag() const { return setBackgroundColor;}

	void setScriptLimitsTag(TagScriptLimits* tag);
	inline TagScriptLimits* getScriptLimitsTag() const { return scriptLimits; }

	void setEnableDebuggerTag(TagEnableDebugger* tag);
	inline TagEnableDebugger* getEnableDebugger() const { return enableDebugger; }

	void setEnableDebuggerTag2(TagEnableDebugger2* tag);
	inline TagEnableDebugger2* getEnableDebugger2() const { return enableDebugger2; }

	void setProductInfoTag(TagProductInfo* tag);
	inline TagProductInfo* getProductInfoTag() const { return productInfo; }

	void setProtectTag(TagProtect* tag);
	inline TagProtect* getProtectTag() const { return protect; }
public:
	void addFrame(SWFFrame* frame);
	void getAllFrames(std::vector<SWFFrame*>& output);
	size_t getFrameCount() const;
public:
	void getImageTags( std::vector<SWFTag*>& output) const;
	void getShapeTags( std::vector<SWFTag*>& output) const;
	void getSpriteTags( std::vector<SWFTag*>& output) const;
	void getFontTags( std::vector<SWFTag*>& output) const;
	void getTextTags( std::vector<SWFTag*>& output) const;
	void getButtonTags( std::vector<SWFTag*>& output) const;
	void getOtherTags( std::vector<SWFTag*>& output) const;
};

class SWFFrame
	: public Object
{
	friend class SWFFrameWrapper;
	friend class SWF;
private:
	SWFFrame();
public:
	CREATE_FUNC(SWFFrame);
	~SWFFrame();
private:
	TagFrameLabel* frameLabel;
	TagSymbolClass* symbolClass;

	std::vector<DefineTag*> exportDefs;
	std::vector<TagDoABC*> doABCs;
	std::vector<TagImportAssets2*> imports;
	std::vector<SWFTag*> controlTags;

	std::vector<DefineTag*> buttons;
	std::vector<DefineTag*> binaryDatas;
	std::vector<DefineTag*> bitmaps;
	std::vector<DefineTag*> fonts;
	std::vector<DefineTag*> morphShapes;
	std::vector<DefineTag*> shapes;
	std::vector<DefineTag*> sounds;
	std::vector<DefineTag*> sprites;
	std::vector<DefineTag*> texts;

public:
	void getAllDefineTags(std::vector<DefineTag*>& tags);

	void setFrameLabel(TagFrameLabel* tag);
	inline TagFrameLabel* getFrameLable() const { return frameLabel; }
	void setSymbolClass(TagSymbolClass* tag);
	inline TagSymbolClass* getSymbolClass() const { return symbolClass; }

	void addDoABCTag(TagDoABC* tag);
	void getAllDoABCTags(std::vector<TagDoABC*>& output) const;

	void addControlTag(SWFTag* tag);
	void getAllControlTags(std::vector<SWFTag*>& output) const;

	void addButtonTag(DefineTag* tag);
	void addBinaryDataTag(DefineTag* tag);
	void addBitmapTag(DefineTag* tag);
	void addFontTag(DefineTag* tag);
	void addMorphShapeTag(DefineTag* tag);
	void addShapeTag(DefineTag* tag);
	void addSoundTag(DefineTag* tag);
	void addSpriteTag(DefineTag* tag);
	void addTextTag(DefineTag* tag);
	void addDefineTag(DefineTag* tag);
};

#endif // __SWF_H__
